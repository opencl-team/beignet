Description: Add LLVM 10 support

Remove -std=c++0x, as LLVM 10 requires at least c++14 (the default)

BasicBlockPass no longer exists; as they suggest, replace it with
FunctionPass with a loop over BasicBlocks
https://github.com/llvm/llvm-project/commit/9f0ff0b2634bab6a5be8dace005c9eb24d386dd1#diff-bddbe5e4c647cb67298584000b67dea1
Return true from IntrinsicLoweringPass as it can modify its input
(possibly a bug before?)

setAlignment now takes a MaybeAlign not a uint

Don't call initializeDominatorTreeWrapperPassPass and
initializeLoopInfoWrapperPassPass, as they no longer exist

Add explicit template initialization to avoid an undefined symbol

Author: Rebecca N. Palmer <rebecca_palmer@zoho.com>

--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -77,7 +77,7 @@ elseif (COMPILER STREQUAL "CLANG")
 elseif (COMPILER STREQUAL "ICC")
   set (CMAKE_C_CXX_FLAGS "${CMAKE_C_CXX_FLAGS}  -wd2928 -Wall -fPIC -fstrict-aliasing -fp-model fast -msse4.1 -Wl,-E")
 endif ()
-set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_C_CXX_FLAGS} -std=c++0x -Wno-invalid-offsetof")
+set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_C_CXX_FLAGS} -Wno-invalid-offsetof")
 set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CMAKE_C_CXX_FLAGS}")
 set (CMAKE_CXX_FLAGS_DEBUG          "-O0 -g -DGBE_DEBUG=1")
 set (CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g -DGBE_DEBUG=1")
--- a/backend/src/backend/program.cpp
+++ b/backend/src/backend/program.cpp
@@ -695,8 +695,12 @@ namespace gbe {
                 );
 
     clang::CompilerInvocation::CreateFromArgs(*CI,
+#if LLVM_VERSION_MAJOR < 10
                                               &args[0],
                                               &args[0] + args.size(),
+#else
+                                              clang::ArrayRef<const char*>(args),
+#endif
                                               Diags);
     // Create the compiler instance
     clang::CompilerInstance Clang;
@@ -1248,8 +1252,12 @@ EXTEND_QUOTE:
       // Create the compiler invocation
       std::unique_ptr<clang::CompilerInvocation> CI(new clang::CompilerInvocation);
       return clang::CompilerInvocation::CreateFromArgs(*CI,
+#if LLVM_VERSION_MAJOR < 10
                                                        &args[0],
                                                        &args[0] + args.size(),
+#else
+                                                       clang::ArrayRef<const char*>(args),
+#endif
                                                        Diags);
     }
 #endif
--- a/backend/src/llvm/llvm_gen_backend.hpp
+++ b/backend/src/llvm/llvm_gen_backend.hpp
@@ -130,10 +130,10 @@ namespace gbe
   llvm::FunctionPass *createGenPass(ir::Unit &unit);
 
   /*! Remove the GEP instructions */
-  llvm::BasicBlockPass *createRemoveGEPPass(const ir::Unit &unit);
+  llvm::FunctionPass *createRemoveGEPPass(const ir::Unit &unit);
 
   /*! Merge load/store if possible */
-  llvm::BasicBlockPass *createLoadStoreOptimizationPass();
+  llvm::FunctionPass *createLoadStoreOptimizationPass();
 
   /*! Scalarize all vector op instructions */
   llvm::FunctionPass* createScalarizePass();
@@ -141,7 +141,7 @@ namespace gbe
   llvm::ModulePass* createBarrierNodupPass(bool);
 
   /*! Convert the Intrinsic call to gen function */
-  llvm::BasicBlockPass *createIntrinsicLoweringPass();
+  llvm::FunctionPass *createIntrinsicLoweringPass();
 
   /*! Passer the printf function call. */
   llvm::FunctionPass* createPrintfParserPass(ir::Unit &unit);
--- a/backend/src/llvm/llvm_intrinsic_lowering.cpp
+++ b/backend/src/llvm/llvm_intrinsic_lowering.cpp
@@ -29,12 +29,12 @@
 using namespace llvm;
 
 namespace gbe {
-    class InstrinsicLowering : public BasicBlockPass
+    class InstrinsicLowering : public FunctionPass
     {
     public:
       static char ID;
       InstrinsicLowering() :
-        BasicBlockPass(ID) {}
+        FunctionPass(ID) {}
 
       void getAnalysisUsage(AnalysisUsage &AU) const {
 
@@ -93,9 +93,9 @@ namespace gbe {
         CI->eraseFromParent();
         return NewCI;
       }
-      virtual bool runOnBasicBlock(BasicBlock &BB)
+      virtual bool runOnFunction(Function &F)
       {
-        bool changedBlock = false;
+        for (BasicBlock &BB : F) {
         Module *M = BB.getParent()->getParent();
 
         DataLayout TD(M);
@@ -159,13 +159,14 @@ namespace gbe {
             }
           }
         }
-        return changedBlock;
+        }
+        return true;
       }
     };
 
     char InstrinsicLowering::ID = 0;
 
-    BasicBlockPass *createIntrinsicLoweringPass() {
+    FunctionPass *createIntrinsicLoweringPass() {
       return new InstrinsicLowering();
     }
 } // end namespace
--- a/backend/src/llvm/llvm_loadstore_optimization.cpp
+++ b/backend/src/llvm/llvm_loadstore_optimization.cpp
@@ -26,13 +26,13 @@
 
 using namespace llvm;
 namespace gbe {
-  class GenLoadStoreOptimization : public BasicBlockPass {
+  class GenLoadStoreOptimization : public FunctionPass {
 
   public:
     static char ID;
     ScalarEvolution *SE;
     const DataLayout *TD;
-    GenLoadStoreOptimization() : BasicBlockPass(ID) {}
+    GenLoadStoreOptimization() : FunctionPass(ID) {}
 
     void getAnalysisUsage(AnalysisUsage &AU) const {
 #if LLVM_VERSION_MAJOR * 10 + LLVM_VERSION_MINOR >= 38
@@ -45,7 +45,9 @@ namespace gbe {
       AU.setPreservesCFG();
     }
 
-    virtual bool runOnBasicBlock(BasicBlock &BB) {
+    virtual bool runOnFunction(Function &F) {
+        bool changedAnyBlock = false;
+        for (BasicBlock &BB : F) {
 #if LLVM_VERSION_MAJOR * 10 + LLVM_VERSION_MINOR >= 38
       SE = &getAnalysis<ScalarEvolutionWrapperPass>().getSE();
 #else
@@ -59,7 +61,9 @@ namespace gbe {
       #else
         TD = getAnalysisIfAvailable<DataLayout>();
       #endif
-      return optimizeLoadStore(BB);
+           changedAnyBlock = optimizeLoadStore(BB) | changedAnyBlock;
+        }
+        return changedAnyBlock;
     }
     Type    *getValueType(Value *insn);
     Value   *getPointerOperand(Value *I);
@@ -148,7 +152,11 @@ namespace gbe {
       values.push_back(merged[i]);
     }
     LoadInst *ld = cast<LoadInst>(merged[0]);
+#if LLVM_VERSION_MAJOR < 10
     unsigned align = ld->getAlignment();
+#else
+    MaybeAlign align = ld->getAlign();
+#endif
     unsigned addrSpace = ld->getPointerAddressSpace();
     // insert before first load
     Builder.SetInsertPoint(ld);
@@ -231,7 +239,11 @@ namespace gbe {
 
     unsigned addrSpace = st->getPointerAddressSpace();
 
+#if LLVM_VERSION_MAJOR < 10
     unsigned align = st->getAlignment();
+#else
+    MaybeAlign align = st->getAlign();
+#endif
     // insert before the last store
     Builder.SetInsertPoint(merged[size-1]);
 
@@ -325,7 +337,7 @@ namespace gbe {
     return changed;
   }
 
-  BasicBlockPass *createLoadStoreOptimizationPass() {
+  FunctionPass *createLoadStoreOptimizationPass() {
     return new GenLoadStoreOptimization();
   }
 };
--- a/backend/src/llvm/llvm_passes.cpp
+++ b/backend/src/llvm/llvm_passes.cpp
@@ -37,7 +37,7 @@
 #include "sys/map.hpp"
 
 using namespace llvm;
-
+template class cfg::Update<BasicBlock *>;
 namespace gbe
 {
   bool isKernelFunction(const llvm::Function &F) {
@@ -219,13 +219,13 @@ namespace gbe
     return offset;
   }
 
-  class GenRemoveGEPPasss : public BasicBlockPass
+  class GenRemoveGEPPasss : public FunctionPass
   {
 
    public:
     static char ID;
     GenRemoveGEPPasss(const ir::Unit &unit) :
-      BasicBlockPass(ID),
+      FunctionPass(ID),
       unit(unit) {}
     const ir::Unit &unit;
     void getAnalysisUsage(AnalysisUsage &AU) const {
@@ -242,16 +242,18 @@ namespace gbe
 
     bool simplifyGEPInstructions(GetElementPtrInst* GEPInst);
 
-    virtual bool runOnBasicBlock(BasicBlock &BB)
+    virtual bool runOnFunction(Function &F)
     {
-      bool changedBlock = false;
+      bool changedAnyBlock = false;
+        for (BasicBlock &BB : F) {
       iplist<Instruction>::iterator I = BB.getInstList().begin();
       for (auto nextI = I, E = --BB.getInstList().end(); I != E; I = nextI) {
         iplist<Instruction>::iterator I = nextI++;
         if(GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(&*I))
-          changedBlock = (simplifyGEPInstructions(gep) || changedBlock);
+          changedAnyBlock = (simplifyGEPInstructions(gep) | changedAnyBlock);
       }
-      return changedBlock;
+        }
+      return changedAnyBlock;
     }
   };
 
@@ -367,7 +369,7 @@ namespace gbe
     return true;
   }
 
-  BasicBlockPass *createRemoveGEPPass(const ir::Unit &unit) {
+  FunctionPass *createRemoveGEPPass(const ir::Unit &unit) {
     return new GenRemoveGEPPasss(unit);
   }
 } /* namespace gbe */
--- a/backend/src/llvm/llvm_sampler_fix.cpp
+++ b/backend/src/llvm/llvm_sampler_fix.cpp
@@ -33,11 +33,13 @@ namespace gbe {
   class SamplerFix : public FunctionPass {
   public:
     SamplerFix() : FunctionPass(ID) {
+#if LLVM_VERSION_MAJOR < 10
 #if LLVM_VERSION_MAJOR * 10 + LLVM_VERSION_MINOR >= 35
       initializeDominatorTreeWrapperPassPass(*PassRegistry::getPassRegistry());
 #else
       initializeDominatorTreePass(*PassRegistry::getPassRegistry());
 #endif
+#endif
     }
 
     bool visitCallInst(CallInst *I) {
--- a/backend/src/llvm/llvm_gen_backend.cpp
+++ b/backend/src/llvm/llvm_gen_backend.cpp
@@ -575,11 +575,13 @@ namespace gbe
         has_errors(false),
         legacyMode(true)
     {
+#if LLVM_VERSION_MAJOR < 10
 #if LLVM_VERSION_MAJOR * 10 + LLVM_VERSION_MINOR >= 37
       initializeLoopInfoWrapperPassPass(*PassRegistry::getPassRegistry());
 #else
       initializeLoopInfoPass(*PassRegistry::getPassRegistry());
 #endif
+#endif
       pass = PASS_EMIT_REGISTERS;
     }
 
--- a/backend/src/llvm/llvm_scalarize.cpp
+++ b/backend/src/llvm/llvm_scalarize.cpp
@@ -96,11 +96,13 @@ namespace gbe {
 
     Scalarize() : FunctionPass(ID)
     {
+#if LLVM_VERSION_MAJOR < 10
 #if LLVM_VERSION_MAJOR * 10 + LLVM_VERSION_MINOR >= 35
       initializeDominatorTreeWrapperPassPass(*PassRegistry::getPassRegistry());
 #else
       initializeDominatorTreePass(*PassRegistry::getPassRegistry());
 #endif
+#endif
     }
 
     virtual bool runOnFunction(Function&);
